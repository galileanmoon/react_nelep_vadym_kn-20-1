import './App.css';
import Cart from './component/Cart';
import Counter from './component/Counter';
import CounterList from './component/CounterList';
import Game from './component/Game';
import ProductPage from './component/ProductPage';

function App() {
  return (
    <div className="App">
      <CounterList/>
      <ProductPage></ProductPage>
      <Cart></Cart>
      <Game/>
    </div>
  );
}
 
export default App;

