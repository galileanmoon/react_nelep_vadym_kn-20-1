import { Button, TableCell, TableRow } from "@mui/material";
import { useState } from "react";

export default function TR({ name, price, min, setTotal, setCount }) {
    const [quantity, SetQuantity] = useState(1)

    const ChangeTotal = (int) => {
        setTotal(price * int)
    }
    const ChangeCountState = (int) => {
        if (quantity > min || int > 0) {
            SetQuantity(quantity + int);
            setCount(int);
            ChangeTotal(int)
        }
    }
    return (
        <TableRow>
            <TableCell>{name}</TableCell>
            <TableCell>{price}</TableCell>
            <TableCell>
                <Button onClick={() => ChangeCountState(-1)}>-</Button>
                {quantity}
                <Button onClick={() => ChangeCountState(1)}>+</Button>
            </TableCell>
            <TableCell>{quantity * price}</TableCell>
        </TableRow>
    );
}