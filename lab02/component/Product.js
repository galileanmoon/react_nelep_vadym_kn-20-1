import { Button } from "@mui/material";

export default function Product({ id, name, resetFunc }) {
    const DeleteFunc = () => {
        resetFunc(id)
    }

    return (
        <>
            <p><b>{name}</b></p>
            <Button onClick={DeleteFunc}>Delete</Button>
        </>
    );
}