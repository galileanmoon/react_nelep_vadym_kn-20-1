import { Button, TextField } from "@mui/material";
import { useState } from "react";

export default function Input({ ChangeState }) {
    const [name, setName] = useState('');

    const handleInputChange = (event) => {
        setName(event.target.value);
    };

    const handleAddClick = () => {
        if (name != '') {
            setName('')
            ChangeState(name);
        }
    };

    return (
        <>
            <TextField id="filled-basic" variant="filled" value={name} onChange={handleInputChange}></TextField>
            <Button variant="contained" onClick={handleAddClick}>Add</Button>
        </>
    );
}