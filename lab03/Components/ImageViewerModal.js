import React from 'react';

function ImageViewerModal({ imageUrl, onClose }) {
  const modalStyles = {
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    maxWidth: '80%',
    maxHeight: '80%',
  };

  const imageStyles = {
    maxWidth: '100%',
    maxHeight: '100%',
    display: 'block',
    margin: '0 auto',
  };

  return (
    <div style={modalStyles}>
      <div>
        <button onClick={onClose}>Close</button>
      </div>
      <img src={imageUrl} alt="Large Image" style={imageStyles} />
    </div>
  );
}

export default ImageViewerModal;
